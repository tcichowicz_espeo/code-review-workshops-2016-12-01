package eu.espeo.workshops.cr.repository;

import java.util.Collection;
import java.util.Optional;

public interface Repository<T extends Entity<K>, K> {

    void add(T entity);
    void update(T entity);
    void remove(K id);
    Collection<T> query(Specification<T> specification);
    Optional<T> findById(K id);
}
