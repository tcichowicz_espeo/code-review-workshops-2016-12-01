package eu.espeo.workshops.cr.util;

public interface RandomNumberGenerator {

    int randomInt(int max);
}
