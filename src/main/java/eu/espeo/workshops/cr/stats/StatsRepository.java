package eu.espeo.workshops.cr.stats;

import eu.espeo.workshops.cr.repository.InMemoryRepository;

import java.util.UUID;

import javax.inject.Named;

@Named
public class StatsRepository extends InMemoryRepository<Stats, UUID> {
}
