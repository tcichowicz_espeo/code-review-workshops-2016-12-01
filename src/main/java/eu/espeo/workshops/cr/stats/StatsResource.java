package eu.espeo.workshops.cr.stats;

import static com.google.common.base.Preconditions.checkArgument;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

import eu.espeo.workshops.cr.jersey.NotFoundExceptionMapper.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;

import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Named
@Scope(SCOPE_PROTOTYPE)
@Path("/stats/{reviewId}")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class StatsResource {

    private final StatsRepository statsRepository;

    @PathParam("reviewId")
    private UUID reviewId;

    @GET
    public Stats get() {
        return statsRepository.findById(reviewId)
                .orElseThrow(NotFoundException::new);
    }

    @PUT
    public Stats update(Stats stats) {
        checkArgument(reviewId.equals(stats.getReviewId()), "Path id does not match entity id");
        statsRepository.update(stats);
        return stats;
    }

    @DELETE
    public void remove() {
        statsRepository.remove(reviewId);
    }
}
